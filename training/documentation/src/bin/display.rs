#![doc(html_logo_url = "crown.svg", html_favicon_url = "favicon.ico")]

use std::fmt; // Import `fmt`

#[derive(Debug)]
struct Complex {
    real: f64,
    imag: f64, 
}

// Similarly, implement `Display` for `Complex`
impl fmt::Display for Complex {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        // Customize so only `real` and `imag` are denoted.
        //write!(f, "real: {}, imag: {}", self.real, self.imag)
        write!(f, "{} + {}i", self.real, self.imag)
    }
}

pub fn main() {
    let comp0 = Complex {real: 3.3, imag: 7.2};
    println!("Display: {}", comp0);
    println!("Debug: {:?}", comp0);

}
