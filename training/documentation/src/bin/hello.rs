#![doc(html_logo_url = "crown.svg", html_favicon_url = "favicon.ico")]

fn main() {
    println!("Hello World!");
}

/// Adds one to the number given.
///
/// # Examples
///
/// ```
/// let arg = 5;
/// let answer = my_crate::add_one(arg);
///
/// assert_eq!(6, answer);
/// ```
pub fn add_one(x: i32) -> i32 {
    x + 1
}
