# html_favicon_url
This form of the doc attribute lets you control the favicon of your docs.

`#![doc(html_favicon_url = "https://example.com/favicon.ico")]` for url link<br>
or<br>
`#![doc(html_favicon_url = "favicon.ico")]` for local link<br>
> When using local link, favicon.ico should be placed in ./target/doc/$RUST_APPLICATION directory.
> For guessing_game application using `cargo doc --all --verbose --target-dir public`, favicon.ico should be placed in ./public/doc/guessing_game/favicon.ico<br>
----
This will put `<link rel="shortcut icon" href="{}">` into your docs, where the string for the attribute goes into the {}.

If you don't use this attribute, there will be no favicon.

