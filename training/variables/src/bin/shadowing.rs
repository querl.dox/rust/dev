// This program first binds x to a value of 5.
// Then it shadows x by repeating let x =, taking the original value and adding 1 so the value of x is then 6. 
// The third let statement also shadows x, multiplying the previous value by 2 to give x a final value of 12. 
fn main() {
    let x = 5;

    let x = x + 1;

    let x = x * 2;

    println!("The value of x is: {}", x);
}

// Shadowing is different from marking a variable as mut, because we’ll get a compile-time error if we accidentally try to reassign to this variable without using the let keyword. 
// By using let, we can perform a few transformations on a value but have the variable be immutable after those transformations have been completed.
//
//The other difference between mut and shadowing is that because we’re effectively creating a new variable when we use the let keyword again, we can change the type of the value but reuse the same name.
// For example, say our program asks a user to show how many spaces they want between some text by inputting space characters, but we really want to store that input as a number:
//
// 
// ```
// let spaces = "   ";
// let spaces = spaces.len();
// ```
