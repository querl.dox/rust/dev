// The variable tup binds to the entire tuple, because a tuple is considered a single compound element. 
fn tup1() {
    let tup: (i32, f64, u8) = (500, 6.4, 1);
}

// This function first creates a tuple and binds it to the variable tup.
// It then uses a pattern with let to take tup and turn it into three separate variables, x, y, and z.
// This is called destructuring, because it breaks the single tuple into three parts.
// Finally, the program prints the value of y, which is 6.4.
fn tup2() {
    let tup = (500, 6.4, 1);

    let (x, y, z) = tup;

    println!("The value of y is: {}", y);
}


// This function creates a tuple, x, and then makes new variables for each element by using their index.
// The first index in a tuple is 0.
fn tup3() {
    let x: (i32, f64, u8) = (500, 6.4, 1);

    let five_hundred = x.0;

    let six_point_four = x.1;

    let one = x.2;
}
