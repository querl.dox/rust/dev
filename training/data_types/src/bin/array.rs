// Another way to have a collection of multiple values is with an array.
// Unlike a tuple, every element of an array must have the same type.

fn main() {
    let a = [1, 2, 3, 4, 5];
    let first = a[0];
    let second = a[1];
}

fn atype() {
    // Here, i32 is the type of each element. After the semicolon, the number 5 indicates the element contains five items.
    let a: [i32; 5] = [1, 2, 3, 4, 5];
}

fn month() {
    let months = ["January", "February", "March", "April", "May", "June", "July",
              "August", "September", "October", "November", "December"];
}
