# Add Examples To Rust Libraries

http://xion.io/category/code.html

In Cargo’s parlance, an example is nothing else but a Rust source code of a standalone executable that typically resides in a single .rs file. All such files should be placed in the examples/ directory, at the same level as src/ and the Cargo.toml manifest itself2.

It is also possible to create examples which are themselves just libraries. Since all you can do with such examples is build them, they don’t provide any additional value over normal tests (and especially doc tests).

Because they are outside of the src/ directory, examples do not become a part of your library’s code, and are not deployed to crates.io.

You can also run cargo build --examples to only compile the examples, without running any kind of tests.
